import React, { useState, useEffect } from 'react';

import { SingleProduct} from './SingleProduct.js';
import { Cart } from './Cart';

import arrowleft from '../img/arrow-left.png'
import arrowright from '../img/arrow-right.png'

function App({visible,showCard}) {

  const [data, setData] = useState({
    count: null,
    next: null,
    prev: null,
    results: []
  })

  const [cart, setCart] = useState([]);

  const getData = async (url) => {
    const res = await fetch(url);
    const json = await res.json();
   
    setData(data => ({
     
      results: [...data.results,...json ]
    }))
  }

  // const loadMore = () => {
  //   getData(data.next)
  // }

  const addToCart = (e,image, name ,artist,description, price, id) => {
  
    e.preventDefault();
    if (!isNaN(price)) {
      const isAvailable = cart.find((item) => id === item.id);

      if (isAvailable) {
        setCart(cart => cart.map(item => {
          if (item.id === id) {
            return {
              ...item,
              count: item.count + 1
            }
          }
          return item
        }))
      } else {
        setCart(cart => [...cart, {
          image,
          name,
          artist,
          description,
          price,
          id,
          count: 1
        }])
      }

    }
    showCard(true);

  }

  useEffect(() => {
    getData('http://localhost:3001/musics');
  }, [])
  return (
    <div>
    <div className="products-header">
      <div className="main-container">
          <p>
            Latest arrivals in musica
          </p>
          <div className="arrow-buttons">
            <button><img src={arrowleft} alt="arrowleft"/></button>
            <button><img src={arrowright} alt="arrowright"/></button>
          </div>
        </div>
      </div>

    <div className="main-container">
      
      <div className="products-list">
        {data.results.map(({ id,productName,artist,description, price, image,url }) => {
         
         return  <SingleProduct
            key={id}
            id={id}
            image={image}
            name={productName}
            artist={artist}
            description={description}
            price={price}
            addToCart={addToCart}
          />
        })}
        {/* {!!data.next &&
          <button
            className="load-more"
            loadMore={loadMore}
          >Load more</button>
        } */}
      </div>

    
    </div>
    {visible&&
    <Cart
          cart={cart}
          setCart={setCart}
        />}
    </div>
    
  );
}

export default App;
