import React, {useState} from 'react'
import ImgComponent from './ImgComponent';
import i1 from './carousel-pictures/cr-img4.jpg';
import i2 from './carousel-pictures/cr-img2.jpg';
import i3 from './carousel-pictures/cr-img3.jpg';
import i4 from './carousel-pictures/cr-img1.jpg';
import i5 from './carousel-pictures/cr-img5.jpg';
import left from './carousel-pictures/left.png';
import right from './carousel-pictures/right.png';

function Slider() {

    let sliderArr = [
    <ImgComponent  src={i1} alt="image1"/>,
    <ImgComponent  src={i2} alt="image2"/>,
    <ImgComponent  src={i3} alt="image3"/>,
    <ImgComponent  src={i4} alt="image4"/>,
    <ImgComponent  src={i5} alt="image5"/>];
    const [x,setX] = useState(0);
    const goLeft = () => {
        // console.log(x);
        x === 0 ? setX(-100 * (sliderArr.length - 1)) : setX(x + 100);
        // setX(x + 100);
    };
    const goRight = () => {
    //    console.log(x);
       // sliderArr.length used so the input can be dynamic
       x === -100 * (sliderArr.length - 1) ? setX(0) : setX(x - 100);
    };

    return (
        <div className = "slider">
            {sliderArr.map((item, index) => {
                return (
                    <div key = {index} className = "slide" style = {{transform:`translateX(${x}%)`}}>
                        {item}
                    </div>
                );
            })}
            <button className = "left" onClick = {goLeft}><img className ="left-btn-img" src = {left} alt="left"></img></button>
            <button className = "right" onClick = {goRight}><img className ="right-btn-img" src = {right} alt="right"></img></button>
        </div>
    );
}

export default Slider
