import React, { useState } from 'react';
import './style.scss'
import Slider from './Slider'
import HeaderComponent from './components/HeaderComponent';
import PublisherComponent from './components/PublisherComponent';
import CheckMusic from './components/CheckMusic';
import Footer from './components/Footer';
import Album from './components/Album';


function App() {
  const [visible,setVisible] = useState(false);
  return (
    <div className="App">    
            <HeaderComponent  visible={visible} showCard={setVisible} />      
            <Slider/>
            <CheckMusic/>
            <Album visible={visible} showCard={setVisible}  />
            <PublisherComponent/>
            <Footer/>
    </div>
  );
}

export default App;
