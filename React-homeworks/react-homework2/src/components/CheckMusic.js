import React from 'react';
import cd from '../img/cd.png'
import headphone from '../img/headphone.png'
import calendar from '../img/calendar.png'

function CheckMusic() {
    return (
        <div>
            <div className = "section-container-3">
              <div className = "main-container">
                <p className = "welcome-to-musica"> WELCOME TO  <span className = "welcome-to-musica-span">MUSICA,</span>  CHECK OUR LATEST ALBUMS</p>
              </div>
            </div>

            <div className = "section-container-4">
              <div className = "main-container">
                 <div className ="box-1">
                    <div className ="title-box-1">
                      <img src={cd} alt ="cd"></img>
                      <p>CHECK OUR CD COLLECTION</p>
                    </div>
                    <div className ="second-title-box-1">
                      <p>Donec pede justo, fringilla vel, al, vulputate  eget, arcu. In enim justo lorem ipsum</p>
                    </div>
                  </div>

                  <div className ="box-2">
                    <div className ="title-box-2">
                      <img src={headphone} alt ="headphone"></img>
                      <p>LISTEN BEFORE PURCHASE</p>
                    </div>
                    <div className ="second-title-box-2">
                      <p>Donec pede justo, fringilla vel, al, vulputate  eget, arcu. In enim justo lorem ipsum</p>
                    </div>
                  </div>

                  <div className ="box-3">
                    <div className ="title-box-3">
                      <img src={calendar} alt ="calendar"></img>
                      <p>UPCOMING EVENTS</p>
                    </div>
                    <div className ="second-title-box-3">
                      <p>Donec pede justo, fringilla vel, al, vulputate  eget, arcu. In enim justo lorem ipsum</p>
                    </div>
                  </div>
              </div>
            </div>
        </div>
    )
}

export default CheckMusic
