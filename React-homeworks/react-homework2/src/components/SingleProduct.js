import React from 'react';
import ReactStars from "react-rating-stars-component";
// import image from '../img/albums/Image.png'

export const SingleProduct = ({ name, image, artist, description, price,addToCart,loadMore,id}) => {
  
    return (
        <div className="single-product" >
            <img src = {image} alt="image1"></img>
            <div className="about-single-product">
                <div className="name-and-artist-container"> 
                    <h4>{name}</h4>
                    <i>{artist}</i>
                </div>
                <ReactStars
                count={5}
                // onChange={this.ratingChanged}
                size={24}
                color1={"#222222"}
                color2={"#d23939"}
                />
                <p>{description}</p>
                <div className="price-and-btn-container">
                    <h3>${price}</h3>
                    <button className="add-to-card-btn" onClick={(e) =>addToCart(e,image, name ,artist,description, price, id)}>Add to card</button>
                </div>
                {/* <button className="load-more" onClick={() => {loadMore()}}>load more</button> */}
            </div>
            
        </div>
        
    )
}
