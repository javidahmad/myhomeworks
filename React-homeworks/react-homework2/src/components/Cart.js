import React from 'react';
import ReactStars from 'react-rating-stars-component';
import rightBtn from '../img/right.png'


export const Cart = ({ cart, setCart }) => {
  
    const removeFromCart = (id, count) => {
        if (count > 1) {
            setCart(cart => cart.map(item => {
                
                if (item.id === id) {
                    return {
                        ...item,
                        count: item.count - 1
                    }
                }
                return item
            }))
        } else {
            
            setCart(cart => cart.filter(item => item.id !== id))
        }
    }
    
    return (
        <div className="cart">
            
            {cart.map(({ name, artist,image, price, id, url,count }) => (
                <div className=" cart-container"  key={id}>
                    <div className = "cart-first-and-starts-container">
                        <div className="cart-first-container">
                            <img width ="57px" height = "59px" src = {image} alt = "mg1"></img>
                            <h4>{count} x {name}</h4>
                            <i>{artist}</i>
                        </div>
                        <div className="cart-stars-container">
                            <ReactStars
                            count={5}
                            // onChange={this.ratingChanged}
                            size={24}
                            color1={"#222222"}
                            color2={"#d23939"}
                            />
                        </div>
                    </div>
                
                 
                 
                    <div className = "cart-second-container">
                        <b>${price}</b>
                        <br/>
                        <button className ="close-x-btn"
                            onClick={() => removeFromCart(id, count)}
                        >X</button>
                    </div>
                    
                </div>
                
            ))}
            <div className ="total-cost-container">
                <h3>Total delivery cost: </h3>
                <h3>${cart.reduce((total, { price, count }) =>(parseFloat(total) + price * count).toFixed(2), 0)}</h3>
            </div>
            <div className="view-and-proceed-buttons">
                <button>View Cart <img src={rightBtn} alt="right btn img" /></button>
                <button>Proceed to Checkout <img src={rightBtn} alt="right btn img"/></button>
            </div>
        </div>
            
    )
}
