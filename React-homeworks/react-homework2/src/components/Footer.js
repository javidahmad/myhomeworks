import React from 'react';
import logo1 from '../img/footer-section/logo1.png';
import logo2 from '../img/footer-section/logo2.png';
import logo3 from '../img/footer-section/logo3.png';
import logo4 from '../img/footer-section/logo4.png';
import logo5 from '../img/footer-section/logo5.png';
import image1 from '../img/footer-section/image.png';
import image2 from '../img/footer-section/image2.png';
import comments from '../img/footer-section/comments.png';

function Footer() {
    return (
        <div>
            <footer>
              <div className = "section-container-first-footer">
                <div className = "main-container">
                    <div className="footer-blogs">
                        <div className = "footer-blog1">
                            <h3>Little about us</h3>
                            <p>Sed posuere consectetur  est at. <br/>
                            Nulla vitae elit libero, a pharetra.<br/> 
                            Lorem ipsum dolor sit amet, conse<br/>
                            ctetuer adipiscing elit.</p>
                            <h3>Socialize with us</h3>
                            <img width="30px" height="30px" src={logo1} alt ="facebook"></img>
                            <img width="30px" height="30px" src={logo2} alt ="twitter"></img>
                            <img width="30px" height="30px" src={logo3} alt ="network"></img>
                            <img width="30px" height="30px" src={logo4} alt ="community"></img>
                            <img width="30px" height="30px" src={logo5} alt ="email"></img>
                        </div>
                        <div className = "footer-blog2">
                            <h3>Our Archives</h3>
                            <p>March 2012</p>
                            <p>February 2012</p>
                            <p>January 2012</p>
                            <p>December 2012</p>
                        </div>
                        <div className = "footer-blog3">
                            <h3>Popular Posts</h3>
                            <div className = "first-post-album">
                                <img className ="first-post-album-img" width = "80px" height="60px" src ={image1} alt="AwokenTheHero"></img>
                                
                                  <div className ="firt-post-album-title">
                                  <p>Great Album</p>
                                  <p className = "comments-12">
                                  <img width="20px" height="20px" src = {comments} alt ="comment-icon"></img>
                                  12 COMMENTS</p>
                                  </div>
                            </div>
                            <div className = "second-post-album">
                                <img className="second-post-album-img" width = "80px" height="60px" src ={image2} alt="BadDreams"></img>
                                
                                  <div className ="second-post-album-title">
                                    <p>Great Album</p>
                                    <p className ="comments-12">
                                    <img width="20px" height="20px" src = {comments} alt ="comment-icon"></img>
                                     12 COMMENTS</p>
                                  </div>
                            </div>
                        </div>  
                        <div className = "footer-blog4">
                                <h3>Search our Site</h3>
                                <input type="text" placeholder="Enter Search..."></input>
                                <h3>Tag Cloud</h3>
                                <button>Audio CD</button>
                                <button>Video</button>
                                <button>Playlist</button>
                                <button>Avantgarde</button>
                                <button>Melancholic</button>
                        </div>  
                    </div>
                </div>
              </div>
              <div className = "section-container-second-footer">
                <div className = "main-container">
                    <div className = "footer-nav-1">
                        <a href="#Home" className="footer-nav-li">Home</a>
                        <a href="#Portfolio" className="footer-nav-li">Portfolio</a>
                        <a href="#Sitemap" className="footer-nav-li">Sitemap</a>
                        <a href="#Contact" className="footer-nav-li">Contact</a>
                    </div>
                    
                        <p className = "footer-nav-p">Musica @2013 by PremiumCoding | All Rights Reserved</p>
                    
                </div>
              </div>
            </footer>
        </div>
    )
}

export default Footer
