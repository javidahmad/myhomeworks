import React from 'react';
import img1 from '../img/publishers-section/img1.png'
import img2 from '../img/publishers-section/img2.png'
import img3 from '../img/publishers-section/img3.png'
import img4 from '../img/publishers-section/img4.png'
import img5 from '../img/publishers-section/img5.png'
import img6 from '../img/publishers-section/img6.png'

function PublisherComponent() {
    return (
        <div className = "section-container-publishers">
              <div className = "main-container">
                <p className = "publishers">OUR MOST IMPORTANT PUBLISHERS</p> 
              </div>
              <div className = "main-container">
                <div className ="publishers--box">
                  <img style = {{marginTop : "20px"}} src = {img1} alt = "WaterMelocious"></img>
                  <p className = "img1-p">WaterMelocious</p>
                </div>
                <div className ="publishers--box">
                  <img style = {{marginTop : "45px"}}src = {img2} alt = "Plantcloud"></img>
                </div>
                <div className ="publishers--box">
                  <img style = {{marginTop : "20px"}} src = {img3} alt = "InspiredTemplate"></img>
                  <p className = "img3-p">InspiredTemplate</p>
                </div>
                <div className ="publishers--box">
                  <img style = {{marginTop : "15px"}} src = {img4} alt = "BirdFly"></img>
                </div>
                <div className ="publishers--box">
                  <img src = {img5} alt = "Mansilhouette"></img>
                </div>
                <div className ="publishers--box">
                  <img style = {{marginTop : "20px"}} src = {img6} alt = "YourLogo"></img>
                  <p className = "img6-p">YourLogo</p>
                </div>
              </div>
            </div>

    )
}

export default PublisherComponent
