const msg_name = 'Name?';
const msg_age = 'Age?';
const msg_cancelled = 'You are not allowed this website';
const msg_welcome = 'Welcome';

const name =prompt(msg_name);
const age = prompt(msg_age);

if(age < 18){
    alert('You are not allowed to visit this website');
}

else if(age >= 18 && age <= 22){
    let isContinue=confirm('Are you sure you want to continue');
    alert(isContinue ? msg_welcome + ', ' + name : msg_cancelled);
   

}
else{
    alert(msg_welcome + ', ' + name);
}