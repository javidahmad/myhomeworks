import React from 'react'
import mLogo from '../img/Vector Smart Object.png'
import fb from '../img/facebook-top-icon.png'
import dribble from '../img/top-dribble-icon.png'
import twitter from '../img/top-twitter-icon.png'
import mail from '../img/top-mail-icon.png'
import vimeo from '../img/top-vimeo-icon.png'
import cart from '../img/cart.png'



function HeaderComponent({visible,showCard}) {
  
    return (
        <div>
            <div className = "upperline"></div>
            <header>
                <div className = "section-container-1">
                  <div className = "main-container">
                    <div className = "social-media">
                      <img src = {fb} alt="facebook" title="Facebook"></img>
                      <img src = {dribble} alt="dribble " title="Dribble"></img>
                      <img src = {twitter} alt="twitter" title="Twitter"></img>
                      <img src = {mail} alt="mail" title="Email"></img>
                      <img src = {vimeo} alt="vimeo" title="Vimeo"></img>
                      
                      
                    </div>

                    <div className = 'login-card-panel'>
                      <p className = 'login-register'>Login/Register</p>
                      {visible&&
                      <button  className = 'card'><img className = "card-img" src ={cart} alt="card"></img>Cart</button>}
                      
                    </div>

                  </div>

              </div>
              <div className = "section-container-2">
                <div className = "main-container">
                  <div className = "logo">
                    <img src = {mLogo} alt="Logo"></img><span className ="m-logo-span">M</span><p className = "store">Store</p>
                  </div>

                  <div className = "navigation">
                    <a href="#HOME" className="nav-li">HOME</a>
                    <a href="#CD's" className="nav-li">CD's</a>
                    <a href="#DVD's" className="nav-li">DVD's</a>
                    <a href="#NEWS" className="nav-li">NEWS</a>
                    <a href="#PORTFOLIO" className="nav-li">PORTFOLIO</a>
                    <a href="#CONTACT" className="nav-li">CONTACT US</a>
                
                
                  </div>
                </div>
              </div>
            </header>
        </div>
    )
}

export default HeaderComponent
